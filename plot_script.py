#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 16:46:07 2022

@author: pw
"""

import n_body_class
import dataVoglis

#%% computation settings

keys = ["QR1", "QR2", "QR3", "QR4"]

x_bins, y_bins, z_bins = 1, 7, 7
x_lim, y_lim, z_lim = 2, 4, 4
neighbours = True



#write path
output_path = 'sigma_Voglis.hdf5'

#plot settings
x_slice = x_bins//2

particles = 'on'

#colormap parameters
vmin_id_diag, vmax_id_diag = -14, -10
vmin_ni_diag, vmax_ni_diag =  -14, -10

vmax_id_nondiag, lin_thresh_id = 1e-11, 1e-14
vmax_ni_nondiag, lin_thresh_ni = 1e-11, 1e-14

close_plot, save, save_path = False, True, 'figures'
#%%

#import output data from simulation
datadict = dataVoglis.datadict
mass_one_particle = dataVoglis.mass_one_particle
velocity_factor_si_units = dataVoglis.velocity_factor_si_units
Rh = dataVoglis.RhVoglis

sigmadict = {}    

for idx, key in enumerate(keys):
    #read from file
    sigma = n_body_class.n_body_data(key, datadict[key])
    sigma.groupname = f'bins_{x_bins}_{y_bins}_{z_bins}_lim_{x_lim}_{y_lim}_{z_lim}_neighbours_{neighbours}'
    sigma.read_sigma(output_path, sigma.groupname)
    
    sigmadict[key]= sigma
    

    #plot
    sigma.plot_suite(vmin_id_diag, vmax_id_diag, 
                        vmax_id_nondiag, lin_thresh_id, 
                        vmax_ni_diag, vmin_ni_diag,
                        vmax_ni_nondiag, lin_thresh_ni, 
                        particles = particles,
                        x_slice = x_slice,
                        close_plot = close_plot, save = save, save_path = save_path)
