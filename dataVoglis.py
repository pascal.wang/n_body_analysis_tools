#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 11:13:35 2022

@author: pw
"""

import numpy as np

#%% load data
datadict =	{
  "QR1": np.loadtxt('N_body_simulations_Tremblin/QR1/restart-index-QR1.dat'),
  "QR2": np.loadtxt('N_body_simulations_Tremblin/QR2/restart-index-QR2.dat'),
  "QR3": np.loadtxt('N_body_simulations_Tremblin/QR3/restart-index-QR3.dat'),
  "QR4": np.loadtxt('N_body_simulations_Tremblin/QR4/restart-index-QR4.dat'),
}

#rescale length unit in position and velocity according to Harsoula email "for length units "The length units in the data files must be all devided by 0.0886 in order to have the same values as in Figure 1 of Voglis et al , 2006"
factorL = 0.0886 #value corresponds to half mass radius in raw data coord.
for i in range(6):
    for key, data in datadict.items():
        data[:,i]/=factorL

#constants and parameters in Voglis 2006 ; conversion to SI units
RhVoglis=10.6 #half mass radius in kpc, cf. Voglis 2006 eq (8)

velocity_norm = 0.9/8316.928134286469 #multiply velocities by this to get same values as Voglis 2006 fig. 3 plot
velocity_factor_si_units = velocity_norm*21.5*RhVoglis*1e3 #to get velocity in m/s cf. Voglis 2006 eq (6) (not counting the 0.0886)

Thmct = 3.086e+19/(21.5*1e3) #Thmct : half mass crossing time in s ; inferred from Voglis 2006 eq (6) (1e3 because km/s ;3.086e+19 = 1 kpc); is roughly Hubble time/300 as in email
G=6.67e-11 #newton grav. constant in SI
total_mass = (2*(RhVoglis*3.086e+19)**3)/(G*Thmct**2) #total galaxy mass in kg, using Thmct = (2*Rh^3/(GM)^(1/2) ; in the text, Voglis uses 5e11 solar mass, which is smaller by a factor ~2
mass_one_particle = total_mass/np.shape(datadict['QR1'])[0] #mass of 1 particle, in kg
