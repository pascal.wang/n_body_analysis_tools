#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 14:55:59 2022

@author: pw
"""

import stress_tensor_func as stf
import numpy as np
import warnings
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import h5py

class n_body_data:
    def __init__(self, name, data):
          
        
        self.name = name
        self.data = data
        print(f'Initialised class for model {self.name}')
            
    def compute_sigma(self, x_bins, y_bins, z_bins, x_lim, y_lim, z_lim, neighbours=True):
    
        #setup the bins for computation 
        self.x_bins, self.y_bins, self.z_bins = x_bins, y_bins, z_bins
        self.xlim, self.ylim, self.zlim = x_lim, y_lim, z_lim
        self.neighbours = neighbours
        
        self.mesh_binx, self.mesh_biny, self.mesh_binz = stf.binner(self.data, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim, pmesh = True)
        self.binsize_x, self.binsize_y, self.binsize_z = self.mesh_binx[1]-self.mesh_binx[0], self.mesh_biny[1]-self.mesh_biny[0], self.mesh_binz[1]-self.mesh_binz[0]
        self.center_binx, self.center_biny, self.center_binz = stf.binner(self.data, x_bins, y_bins, z_bins, x_lim=self.mesh_binx[-1]-self.binsize_x/2, y_lim=self.mesh_biny[-1]-self.binsize_y/2, z_lim=self.mesh_binz[-1]-self.binsize_z/2, pmesh = False)  #at the center of the bin                                            
        
        self.groupname = f'bins_{self.x_bins}_{self.y_bins}_{self.z_bins}_lim_{self.xlim}_{self.ylim}_{self.zlim}_neighbours_{self.neighbours}'
        #setup the polar angles
        y_grid, z_grid = np.meshgrid(self.center_biny, self.center_binz)
        #because of the stress tensor coordinate conventions used in the stress_tensor_func, have to transpose for the operations and retranspose for pcolormesh
        self.theta = (np.arctan2(z_grid, y_grid)).transpose()
    
    
        print(f'Computing the stress tensor of model {self.name} ...')
        print(f'with parameters in (x,y,z) \n - bins : ({self.x_bins},{self.y_bins},{self.z_bins}) \n - lim : ({self.xlim},{self.ylim},{self.zlim}) \n - neighbours : {self.neighbours}')
        
        #ideal part
        print(f'Computing ideal part of dataset {self.name} ...')
        self.sigma_id_xx = stf.ideal_stress_tensor(self.data, 0, 0, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim)
        self.sigma_id_yy = stf.ideal_stress_tensor(self.data, 1, 1, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim)
        self.sigma_id_zz = stf.ideal_stress_tensor(self.data, 2, 2, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim)
        
        self.sigma_id_yz = stf.ideal_stress_tensor(self.data, 1, 2, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim)
        
        self.trace_sigma_id = self.sigma_id_xx + self.sigma_id_yy + self.sigma_id_zz
        self.sigma_id_rtheta = np.cos(2*self.theta)*self.sigma_id_yz + 1/2*np.sin(2*self.theta)*(self.sigma_id_zz-self.sigma_id_yy)
        
        
        print(f'Finished computing ideal part of dataset {self.name}.')
        
        #nonideal part
        
        
        print(f'Computing non-ideal part of dataset {self.name} ...')
        self.sigma_ni_xx = stf.non_ideal_stress_tensor(self.data, 0, 0, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim, neighbours = neighbours)
        print('1/4 completed')
        self.sigma_ni_yy = stf.non_ideal_stress_tensor(self.data, 1, 1, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim, neighbours = neighbours) 
        print('2/4 completed')
        self.sigma_ni_zz = stf.non_ideal_stress_tensor(self.data, 2, 2, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim, neighbours = neighbours)
        print('3/4 completed')
        self.sigma_ni_yz = stf.non_ideal_stress_tensor(self.data, 1, 2, x_bins, y_bins, z_bins, x_lim=x_lim, y_lim=y_lim, z_lim=z_lim, neighbours = neighbours)
        print('4/4 completed')
        
        self.trace_sigma_ni = self.sigma_ni_xx + self.sigma_ni_yy + self.sigma_ni_zz
        self.sigma_ni_rtheta = np.cos(2*self.theta)*self.sigma_ni_yz + 1/2*np.sin(2*self.theta)*(self.sigma_ni_zz-self.sigma_ni_yy)
        
        print(f'Finished computing non-ideal part of dataset {self.name}.')
        print(f'COMPUTATION FINISHED for dataset {self.name}.')
     
     
    def read_sigma(self, sigma_path, groupname):
         
        with h5py.File(sigma_path, 'r') as file:
             
            sigma = file.get(f'{self.name}/{groupname}')
            
            self.x_bins, self.y_bins, self.z_bins = sigma.attrs['xbins'], sigma.attrs['ybins'], sigma.attrs['zbins'] 
            self.xlim, self.ylim, self.zlim = sigma.attrs['xlim'], sigma.attrs['ylim'], sigma.attrs['zlim']
            self.neighbours = sigma.attrs['neighbours']
            
            self.mesh_binx, self.mesh_biny, self.mesh_binz = stf.binner(self.data, self.x_bins, self.y_bins, self.z_bins, x_lim=self.xlim, y_lim=self.ylim, z_lim=self.zlim, pmesh = True)
            self.binsize_x, self.binsize_y, self.binsize_z = self.mesh_binx[1]-self.mesh_binx[0], self.mesh_biny[1]-self.mesh_biny[0], self.mesh_binz[1]-self.mesh_binz[0]
            self.center_binx, self.center_biny, self.center_binz = stf.binner(self.data, self.x_bins, self.y_bins, self.z_bins, x_lim=self.mesh_binx[-1]-self.binsize_x/2, y_lim=self.mesh_biny[-1]-self.binsize_y/2, z_lim=self.mesh_binz[-1]-self.binsize_z/2, pmesh = False)  #at the center of the bin                                   

            self.trace_sigma_id = np.array(sigma['trace_sigma_id'])
            self.sigma_id_yz = np.array(sigma['sigma_id_yz'])
            self.sigma_id_rtheta = np.array(sigma['sigma_id_rtheta'])
            
            self.trace_sigma_ni = np.array(sigma['trace_sigma_ni'])
            self.sigma_ni_yz = np.array(sigma['sigma_ni_yz'])
            self.sigma_ni_rtheta = np.array(sigma['sigma_ni_rtheta'])
            
            file.close()
         
         

   
    
   
    def convert_to_si_units(self, mass_one_particle, velocity_factor_si_units, Rh):
        #ideal part
        ideal_units_factor = mass_one_particle*(velocity_factor_si_units)**2/(Rh*3.086e+19)**3
        
        self.sigma_id_xx *= ideal_units_factor
        self.sigma_id_yy *= ideal_units_factor
        self.sigma_id_zz *= ideal_units_factor
        
        self.sigma_id_yz *= ideal_units_factor
        
        self.trace_sigma_id *= ideal_units_factor
        self.sigma_id_rtheta *= ideal_units_factor
        
        #non ideal part
        
        non_ideal_units_factor  = (mass_one_particle)**2/(Rh*3.086e+19)/(Rh*3.086e+19)**3
        
        self.sigma_ni_xx *= non_ideal_units_factor
        self.sigma_ni_yy *= non_ideal_units_factor
        self.sigma_ni_zz *= non_ideal_units_factor
        
        self.sigma_ni_yz *= non_ideal_units_factor
        
        self.trace_sigma_ni *= non_ideal_units_factor
        self.sigma_ni_rtheta *= non_ideal_units_factor
        
            
    def write_sigma(self, output_path):
        
        #write data to hdf5 file
        with h5py.File(output_path, 'a') as file:
            
            model = file.require_group(self.name)
            
            
            
            if self.groupname in model:
                print("Output already written. Not overwriting.")
                
            else:
                sigma = model.require_group(self.groupname)
                
                sigma.attrs['model'] = self.name
                
                sigma.attrs['xbins'], sigma.attrs['ybins'], sigma.attrs['zbins'] = self.x_bins, self.y_bins, self.z_bins
                sigma.attrs['xlim'], sigma.attrs['ylim'], sigma.attrs['zlim'] =  self.xlim, self.ylim, self.zlim
                #sigma.attrs['mesh_binx'], sigma.attrs['mesh_biny'], sigma.attrs['mesh_binz'] =   self.mesh_binx, self.mesh_biny, self.mesh_binz
                
                sigma.attrs['neighbours'] = self.neighbours
                
                sigma.require_dataset('trace_sigma_id', data = self.trace_sigma_id, shape = self.trace_sigma_id.shape, dtype = self.trace_sigma_id.dtype)
                sigma.require_dataset('sigma_id_yz', data = self.sigma_id_yz, shape = self.sigma_id_yz.shape, dtype = self.sigma_id_yz.dtype)
                sigma.require_dataset('sigma_id_rtheta', data = self.sigma_id_rtheta, shape = self.sigma_id_rtheta.shape, dtype = self.sigma_id_rtheta.dtype)
                
                sigma.require_dataset('trace_sigma_ni', data = self.trace_sigma_ni, shape = self.trace_sigma_ni.shape, dtype = self.trace_sigma_ni.dtype)
                sigma.require_dataset('sigma_ni_yz', data = self.sigma_ni_yz, shape = self.sigma_ni_yz.shape, dtype = self.sigma_ni_yz.dtype)
                sigma.require_dataset('sigma_ni_rtheta', data = self.sigma_ni_rtheta, shape = self.sigma_ni_rtheta.shape, dtype = self.sigma_ni_rtheta.dtype)
                
                print(f'Wrote data to file {output_path} for model {self.name} in group "{self.groupname}"')
             
            file.close()
            

    def data_plotter(self, data_to_plot, vmin=None, vmax=None, cmap=None, norm = None, title = None, x_slice = 0, particles = 'on', save_path = '', save_label = '', save = False):
        plt.figure(figsize=(7,7))
        
        cmesh = plt.pcolormesh(self.mesh_biny,self.mesh_binz, data_to_plot, norm=norm, cmap = cmap, vmin = vmin, vmax=vmax)
        clb=plt.colorbar(cmesh)
        clb.ax.set_title(title)
        
        if particles == 'on':
            plt.scatter(self.data[(self.mesh_binx[x_slice]<self.data[:,0]) & (self.data[:,0]<self.mesh_binx[x_slice+1]), 1], self.data[(self.mesh_binx[x_slice]<self.data[:,0]) & (self.data[:,0]<self.mesh_binx[x_slice+1]), 2], s=1, c='black', linewidths =0, marker='o', alpha=0.2)
        
        plt.title(self.name+rf', control volume of size {self.binsize_x:.1f}$\times${self.binsize_y:.1f}$\times${self.binsize_z:.1f} '+r'$R_h$')
        plt.xlim(-self.ylim, self.ylim)
        plt.ylim(-self.zlim, self.zlim)
        plt.xlabel(r'y [$R_h$]')
        plt.ylabel(r'z [$R_h$]')
        if save:
            plt.savefig(f'{save_path}/{save_label}_{self.name}_{self.groupname}.png', dpi = 300, bbox_inches='tight')
        
        
        
    def plot_suite(self, vmin_id_diag, vmax_id_diag, 
                         vmax_id_nondiag, lin_thresh_id, 
                         vmax_ni_diag, vmin_ni_diag,
                         vmax_ni_nondiag, lin_thresh_ni, 
                         x_slice = 0, particles = 'on', close_plot = False, save = False, save_path = None):
        
        #ideal diagonal
        title = r'$\log tr(\sigma^{id})$'
        save_label = 'trace_sigma_id/trace_sigma_id'
        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='divide by zero encountered in log10')
            self.data_plotter(np.log10(self.trace_sigma_id[x_slice,:,:]).transpose(), cmap = 'Reds', vmin = vmin_id_diag, vmax=vmax_id_diag, title = title, save = save, save_label=save_label, save_path=save_path, x_slice = x_slice)
        
        #ideal nondiagonal (rtheta)
        title=r'$\sigma_{r\theta}^{id}$'
        save_label = 'sigma_id_rtheta/sigma_id_rtheta'
        self.data_plotter(self.sigma_id_rtheta[x_slice,:,:].transpose(), norm=colors.SymLogNorm(linthresh = lin_thresh_id, vmax = vmax_id_nondiag, vmin= -vmax_id_nondiag), title = title, cmap='RdBu_r', save = save, save_label=save_label, save_path=save_path, x_slice = x_slice)
        
        #ideal nondiagonal (yz)
        title=r'$\sigma_{yz}^{id}$'
        save_label = 'sigma_id_yz/sigma_id_yz'
        self.data_plotter(self.sigma_id_yz[x_slice,:,:].transpose(), norm=colors.SymLogNorm(linthresh = lin_thresh_id, vmax = vmax_id_nondiag, vmin= -vmax_id_nondiag), title = title, cmap='RdBu_r', save = save, save_label=save_label, save_path=save_path, x_slice = x_slice)
        
        #nonideal diagonal
        title = r'$\log tr(\sigma^{ni})$'
        save_label = 'trace_sigma_ni/trace_sigma_ni'
        with warnings.catch_warnings():
            warnings.filterwarnings(action='ignore', message='divide by zero encountered in log10')
            self.data_plotter(np.log10(self.trace_sigma_ni[x_slice,:,:]).transpose(), cmap = 'Reds', vmin = vmin_ni_diag, vmax=vmax_ni_diag, title = title,  save = save, save_label=save_label, save_path=save_path, x_slice = x_slice)
    
        #nonideal nondiagonal (rtheta)
        title=r'$\sigma_{r\theta}^{ni}$'
        save_label = 'sigma_ni_rtheta/sigma_ni_rtheta'
        self.data_plotter(self.sigma_ni_rtheta[x_slice,:,:].transpose(), norm=colors.SymLogNorm(linthresh = lin_thresh_ni, vmax = vmax_ni_nondiag, vmin= -vmax_ni_nondiag), title = title, cmap='RdBu_r',  save = save, save_label=save_label, save_path=save_path, x_slice = x_slice)
        
        #nonideal nondiagonal (yz)
        title=r'$\sigma_{yz}^{ni}$'
        save_label = 'sigma_ni_yz/sigma_ni_yz'
        self.data_plotter(self.sigma_ni_yz[x_slice,:,:].transpose(), norm=colors.SymLogNorm(linthresh = lin_thresh_ni, vmax = vmax_ni_nondiag, vmin= -vmax_ni_nondiag), title = title, cmap='RdBu_r', save = save, save_label=save_label, save_path=save_path, x_slice = x_slice)
        
        if close_plot :
            plt.close('all')
        
        #angle test
        
        """
        plt.figure(figsize=(6,6))
        cmesh=plt.pcolormesh(self.mesh_biny, self.mesh_binz, self.theta)
        clb=plt.colorbar(cmesh)
        clb.ax.set_title(r'$\log tr(\sigma^{id})$')
        
        plt.scatter(self.data[(self.mesh_binx[x_slice]<self.data[:,0]) & (self.data[:,0]<self.mesh_binx[x_slice+1]), 1], self.data[(self.mesh_binx[x_slice]<self.data[:,0]) & (self.data[:,0]<self.mesh_binx[x_slice+1]), 2], s=1, c='black', linewidths =0, marker='o', alpha=0.2)
        
        plt.xlim(-self.ylim, self.ylim)
        plt.ylim(-self.zlim, self.zlim)
        plt.xlabel(r'y [$R_h$]')
        plt.ylabel(r'z [$R_h$]')
    
        """
        
    def comparison_plot(self, fignum = None, x_slice=0, color_idx=0, legend = True):
        plt.figure(fignum)
        
        
        r_bin_trace, radial_profile_trace = stf.color_plot_to_radial_profile(self.center_biny, self.center_binz, self.trace_sigma_id[x_slice,:,:] )
        if legend:
            label = self.name+r' tr $\sigma^{id}$'
        else:
            label = self.name
        plt.plot(r_bin_trace, radial_profile_trace, label = label, color="C"+f'{color_idx}')
        
        r_bin, radial_profile = stf.color_plot_to_radial_profile(self.center_biny, self.center_binz,   self.sigma_id_rtheta[x_slice,:,:] )
        if legend:
            label = self.name+  r' $\sigma^{id}_{r\theta}$'
        else:
            label = None
        plt.plot(r_bin, np.abs(radial_profile), label = label, color="C"+f'{color_idx}', linestyle='dotted')
        
        r_bin_trace, radial_profile_trace = stf.color_plot_to_radial_profile(self.center_biny, self.center_binz,   self.trace_sigma_ni[x_slice,:,:] )
        if legend:
            label = self.name+r' tr $\sigma^{ni}$'
        else:
            label = None
        plt.plot(r_bin_trace, radial_profile_trace, label = label, color="C"+f'{color_idx}', linestyle = 'dashed')
        
        r_bin, radial_profile = stf.color_plot_to_radial_profile(self.center_biny, self.center_binz,   self.sigma_ni_rtheta[x_slice,:,:])
        if legend:
            label = self.name+  r' $\sigma^{ni}_{r\theta}$'
        else:
            label = None
        plt.plot(r_bin, np.abs(radial_profile), label = label, color="C"+f'{color_idx}', linestyle='dashdot')
        
        
        plt.yscale('log')
        plt.legend()
        plt.xlabel(r'r [$R_h$]')
        plt.ylabel(r'$\sigma$ [Pa]')
        plt.title(rf'Control volume of size {self.binsize_x:.1f}$\times${self.binsize_y:.1f}$\times${self.binsize_z:.1f} '+r'$R_h$')
        plt.tight_layout()   

        
        
    """
    def comparison_plot_former(self, fignum = None, x_slice=0, color_idx=0):
        plt.figure(fignum)
        
        radial_bin_number = np.min([50, self.y_bins, self.z_bins])
        
        r_bin_trace, radial_profile_trace = stf.color_plot_to_radial_profile(self.mesh_biny, self.mesh_binz, self.data, self.trace_sigma_id[x_slice,:,:], bin_number=radial_bin_number, plot_lim=np.minimum(self.ylim, self.zlim))
        label = self.name+r' tr $\sigma^{id}$'
        plt.plot(r_bin_trace, radial_profile_trace, label = label, color="C"+f'{color_idx}')
        
        r_bin, radial_profile = stf.color_plot_to_radial_profile(self.mesh_biny, self.mesh_binz, self.data, self.sigma_id_rtheta[x_slice,:,:], bin_number=radial_bin_number, plot_lim=np.minimum(self.ylim, self.zlim))
        label = self.name+  r' $\sigma^{id}_{r\theta}$'
        plt.plot(r_bin, np.abs(radial_profile), label = label, color="C"+f'{color_idx}', linestyle='dotted')
        
        r_bin_trace, radial_profile_trace = stf.color_plot_to_radial_profile(self.mesh_biny, self.mesh_binz, self.data, self.trace_sigma_ni[x_slice,:,:], bin_number=radial_bin_number, plot_lim=np.minimum(self.ylim, self.zlim))
        label = self.name+r' tr $\sigma^{ni}$'
        plt.plot(r_bin_trace, radial_profile_trace, label = label, color="C"+f'{color_idx}', linestyle = 'dashed')
        
        r_bin, radial_profile = stf.color_plot_to_radial_profile(self.mesh_biny, self.mesh_binz, self.data, self.sigma_ni_rtheta[x_slice,:,:], bin_number=radial_bin_number, plot_lim=np.minimum(self.ylim, self.zlim))
        label = self.name+  r' $\sigma^{ni}_{r\theta}$'
        plt.plot(r_bin, np.abs(radial_profile), label = label, color="C"+f'{color_idx}', linestyle='dashdot')
        
        
        plt.yscale('log')
        plt.legend()
        plt.xlabel(r'r [$R_h$]')
        plt.ylabel(r'$\sigma$ [Pa]')
        plt.title(rf'Control volume of size {self.binsize_x:.1f}$\times${self.binsize_y:.1f}$\times${self.binsize_z:.1f} '+r'$R_h$')
        plt.tight_layout()    
       """     

        
"""    
def data_plotter(data_to_plot, vmin=None, vmax=None, cmap=None, norm = None, title = None, x_slice = 0, particles = 'on', savepath = '', save = True):
            plt.figure(figsize=(7,7))
            
            cmesh = plt.pcolormesh(self.mesh_biny,self.mesh_binz, data_to_plot, norm=norm, cmap = cmap, vmin = vmin, vmax=vmax)
            clb=plt.colorbar(cmesh)
            clb.ax.set_title(title)
            
            if particles == 'on':
                plt.scatter(self.data[(self.mesh_binx[x_slice]<self.data[:,0]) & (self.data[:,0]<self.mesh_binx[x_slice+1]), 1], self.data[(self.mesh_binx[x_slice]<self.data[:,0]) & (self.data[:,0]<self.mesh_binx[x_slice+1]), 2], s=1, c='black', linewidths =0, marker='o', alpha=0.2)
            
            plt.title(self.name+rf', control volume of size {self.binsize_x:.1f}$\times${self.binsize_y:.1f}$\times${self.binsize_z:.1f} '+r'$R_h$')
            plt.xlim(-self.ylim, self.ylim)
            plt.ylim(-self.zlim, self.zlim)
            plt.xlabel(r'y [$R_h$]')
            plt.ylabel(r'z [$R_h$]')
        
            
            
    def full_output_to_plot(output_path, modelname, groupename,
                       vmin_id_diag, vmax_id_diag, 
                        vmax_id_nondiag, lin_thresh_id, 
                        vmax_ni_diag, vmin_ni_diag,
                        vmax_ni_nondiag, lin_thresh_ni, 
                        x_slice = 0, particles = 'on', save = True):
        
        with h5py.File(output_path, 'r') as file:
            
            #get info from plot
            sigma = file.get(f'{modelname}/{groupename}')
            
            assert modelname == sigma.attrs['model']
            
            xbins, ybins, zbins = sigma.attrs['xbins'], sigma.attrs['ybins'], sigma.attrs['zbins'] 
            xlim, ylim, zlim = sigma.attrs['xlim'], sigma.attrs['ylim'], sigma.attrs['zlim']
            binx, biny, binz = sigma.attrs['mesh_binx'], sigma.attrs['mesh_biny'], sigma.attrs['mesh_binz']
                    
"""
           
"""
    def compute_stress_tensor(kind, component, x_bins, y_bins, z_bins, rtheta = True, x_lim=None, y_lim=None, z_lim=None, models='all'):
        
        if models=='all':
            
        else:
            
        #setup the bins
        binx, biny, binz = binner(data, x_bins, y_bins, z_bins, x_lim, y_lim, z_lim, pmesh = False)
        
        kind_list = ['ideal', 'nonideal']
        component_list = ['trace', 'yz', 'rtheta'] 
        
            
        if kind not in kind_list:
            raise ValueError(f'"kind" argument not in {kind_list}')
        
        elif kind == kind_list[0] : #ideal case
            if component not in component_list:
                raise ValueError(f'"component" argument not in {component_list}')
            elif component == component_list[0]: #trace case
            
            
        
            elif component == component_list[1]: #yz case
            
            elif component == component_list[2]: #rtheta case
        
        
        
            
        
        elif kind == kind_list[1] : #nonideal case 

def full_suite(self, x_bins, y_bins, z_bins,  x_lim=None, y_lim=None, z_lim=None, models = 'all'):
    
    if models is not 'all': #models is then a list of values like ["QR1", "QR2"]
        key_list = models
        data_list = [self.datadict[key] for key in key_list]
    else: 
        key_list = list(self.datadict.keys())
        data_list = list(self.datadict.values())
        
     for data_idx, (key, data) in enumerate(zip(key_list, data_list)):
"""    