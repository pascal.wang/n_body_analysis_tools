#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 14:59:11 2022

@author: pw
"""

# (COMPUTING FUNCTIONS) computing virial stress sigma_ij  and other functions

"""
sigma_ij=sigma_ideal_ij + sigma_non_ideal_ij
sigma_ideal_ij = 1/V sum m(u_i(k)-bar u_i)(u_j(k)-bar u_j)
sigma_nonideal_ij = 1/2V sum x_i(l)-x_i(k) f_j(kl)
"""
import numpy as np
import numba as nb
import warnings


@nb.jit(parallel=True)
def ideal_stress_tensor(data, i, j, x_bins, y_bins, z_bins, x_lim=None, y_lim=None, z_lim=None):
    """
    x_bins, y_bins, z_bins give the number of bins
    computes sigma_ideal_ij = 1/V sum m(u_i(k)-bar u_i)(u_j(k)-bar u_j)
    assuming mass = 1 unity
    inside a bin volume
    """
    
    #index for velocities
    i_vel = i+3
    j_vel = j+3
    
    #set up the bins
    if x_lim == None:
        x_max=np.max(data[:,0])
        x_min=np.min(data[:,0])
    else :
        x_max=x_lim
        x_min=-x_lim
        
    binx = np.linspace(x_min, x_max, x_bins+1)
    x_step = binx[1]-binx[0]
    
    if y_lim == None:
        y_max=np.max(data[:,1])
        y_min=np.min(data[:,1])
    else:
        y_max=y_lim
        y_min=-y_lim
    
    biny = np.linspace(y_min, y_max, y_bins+1)
    y_step = biny[1]-biny[0]
    
    if z_lim == None:
        z_max=np.max(data[:,2])
        z_min=np.min(data[:,2])
    else:
        z_max=z_lim
        z_min=-z_lim
    
    binz = np.linspace(z_min, z_max, z_bins+1)
    z_step = binz[1]-binz[0]
    
    #elementary bin volume
    d_volume = x_step*y_step*z_step
    
    #compute ideal stress tensor
    ideal_stress_tensor_array = np.zeros((x_bins, y_bins, z_bins))
    
    for x_idx in nb.prange(x_bins):
        for y_idx in nb.prange(y_bins):
            for z_idx in nb.prange(z_bins):
                
                #select data in the given spatial bin
                subdata = data[(data[:,0]> binx[x_idx]) &
                               (data[:,0]< binx[x_idx+1]) &
                               (data[:,1]> biny[y_idx]) &
                               (data[:,1]< biny[y_idx+1]) &
                               (data[:,2]> binz[z_idx]) &
                               (data[:,2]< binz[z_idx+1] ), :]
                
                if subdata.size > 0:
                    
                    #average velocities
                 
                    bar_u_i=np.mean(subdata[:,i_vel])
                    bar_u_j=np.mean(subdata[:,j_vel])
                 
                    
                    #ideal stress tensor value
                    ideal_stress_tensor_array[x_idx,y_idx,z_idx]= np.sum((subdata[:,i_vel]-bar_u_i)*(subdata[:,j_vel]-bar_u_j))/d_volume
                    
    return ideal_stress_tensor_array

@nb.jit(parallel=True)
def sigma_id_r_theta(data, x_bins, y_bins, z_bins, x_lim=None, y_lim=None, z_lim=None):
    """
    compute off diagonal value of the ideal stress tensor in cylindrical coordinates
    """
    #set up the bins
    if x_lim == None:
        x_max=np.max(data[:,0])
        x_min=np.min(data[:,0])
    else :
        x_max=x_lim
        x_min=-x_lim
        
    binx = np.linspace(x_min, x_max, x_bins+1)
    x_step = binx[1]-binx[0]
    
    if y_lim == None:
        y_max=np.max(data[:,1])
        y_min=np.min(data[:,1])
    else:
        y_max=y_lim
        y_min=-y_lim
    
    biny = np.linspace(y_min, y_max, y_bins+1)
    y_step = biny[1]-biny[0]
    
    if z_lim == None:
        z_max=np.max(data[:,2])
        z_min=np.min(data[:,2])
    else:
        z_max=z_lim
        z_min=-z_lim
    
    binz = np.linspace(z_min, z_max, z_bins+1)
    z_step = binz[1]-binz[0]
    
    #elementary bin volume
    d_volume = x_step*y_step*z_step
    
    #compute ideal stress tensor
    ideal_stress_tensor_array = np.zeros((x_bins, y_bins, z_bins))
    
    for x_idx in nb.prange(x_bins):
        for y_idx in nb.prange(y_bins):
            for z_idx in nb.prange(z_bins):
                
                #select data in the given spatial bin
                subdata = data[(data[:,0]>= binx[x_idx]) &
                               (data[:,0]<= binx[x_idx+1]) &
                               (data[:,1]> biny[y_idx]) &
                               (data[:,1]< biny[y_idx+1]) &
                               (data[:,2]> binz[z_idx]) &
                               (data[:,2]< binz[z_idx+1] ), :]
                
                if subdata.size> 0:
                    
                    #compute polar angle
                    
                    """
                    previous version : coordinates are off by half a bin !!!!
                    
                    theta = np.arctan(binz[z_idx]/biny[y_idx])
                    
                   
                    if biny[y_idx]<0:
                        theta+=np.pi
                    else:
                        if binz[z_idx]<0:
                            theta+=2*np.pi
                    """
                    z_coord = (binz[z_idx]+binz[z_idx+1])/2
                    y_coord = (biny[y_idx]+biny[y_idx+1])/2
                    
                    theta = np.arctan2(z_coord, y_coord)
                    print(theta)
                    #print(theta, np.arctan2(binz[z_idx],biny[y_idx]), np.arctan2(binz[z_idx],biny[y_idx])+np.pi*2)
                    
                    #compute velocities
                    u_r = subdata[:,4]*np.cos(theta)+subdata[:,5]*np.sin(theta)
                    u_theta = -subdata[:,4]*np.sin(theta)+subdata[:,5]*np.cos(theta)
                    
                    ideal_stress_tensor_array[x_idx,y_idx,z_idx] = np.sum((u_r-np.mean(u_r))*(u_theta-np.mean(u_theta)))/d_volume
                    
                    
                    """
                    alternative : use the theta associated with the particles, not the box ; could give different results
                    
                    alternative : change variables at the tensor level : should give same result, but computation is longer
                    
                    print("alternative computation")
                
                    bar_u_y=np.mean(subdata[:,4])
                    bar_u_z=np.mean(subdata[:,5])
                    
                    sigma_yz = np.sum((subdata[:,4]-bar_u_y)*(subdata[:,5]-bar_u_z))/d_volume
                    sigma_yy = np.sum((subdata[:,4]-bar_u_y)*(subdata[:,4]-bar_u_y))/d_volume
                    sigma_zz = np.sum((subdata[:,5]-bar_u_z)*(subdata[:,5]-bar_u_z))/d_volume
                    
                    ideal_stress_tensor_array[x_idx,y_idx,z_idx]= np.cos(2*theta)*sigma_yz + 1/2*np.sin(2*theta)*(sigma_zz-sigma_yy)
                    """

                    
    return ideal_stress_tensor_array

@nb.jit(parallel=True)
def non_ideal_stress_tensor(data, i, j, x_bins, y_bins, z_bins, neighbours = False, x_lim=None, y_lim=None, z_lim=None, G=6.67e-11):
    
    """
    compute non ideal part of the stress tensor
    sigma_nonideal_ij = 1/2V sum x_i(l)-x_i(k) f_j(kl)
    mass is assumed to be unity !!!
    """
    
    #set up the bins
    if x_lim is None:
        x_max=np.max(data[:,0])
        x_min=np.min(data[:,0])
    else :
        x_max=x_lim
        x_min=-x_lim
        
    binx = np.linspace(x_min, x_max, x_bins+1)
    x_step = binx[1]-binx[0]
        
    if y_lim is None:
        y_max=np.max(data[:,1])
        y_min=np.min(data[:,1])
    else:
        y_max=y_lim
        y_min=-y_lim
    
    biny = np.linspace(y_min, y_max, y_bins+1)
    y_step = biny[1]-biny[0]
    
    if z_lim is None:
        z_max=np.max(data[:,2])
        z_min=np.min(data[:,2])
    else:
        z_max=z_lim
        z_min=-z_lim
    
    binz = np.linspace(z_min, z_max, z_bins+1)
    z_step = binz[1]-binz[0]
    
    #compute non ideal stress tensor
    if neighbours:
        int_range_x = x_step/2
        int_range_y = y_step/2
        int_range_z = z_step/2
    
    #elementary bin volume
    d_volume = x_step*y_step*z_step
    
    #compute non ideal stress tensor
    non_ideal_stress_tensor_array = np.zeros((x_bins, y_bins, z_bins))

    
    for x_idx in nb.prange(x_bins):
        for y_idx in nb.prange(y_bins):
            for z_idx in nb.prange(z_bins):
                
                #select data in the given spatial bin
                vol_subdata_indices = np.nonzero((data[:,0]> binx[x_idx]) &
                               (data[:,0]< binx[x_idx+1]) &
                               (data[:,1]> biny[y_idx]) &
                               (data[:,1]< biny[y_idx+1]) &
                               (data[:,2]> binz[z_idx]) &
                               (data[:,2]< binz[z_idx+1]))[0]
                
          
                if neighbours:
                    int_subdata_indices = np.nonzero((data[:,0]> binx[x_idx]-int_range_x) &
                                    (data[:,0]< binx[x_idx+1]+int_range_x) &
                                    (data[:,1]> biny[y_idx]-int_range_y) &
                                    (data[:,1]< biny[y_idx+1]+int_range_y) &
                                    (data[:,2]> binz[z_idx]-int_range_z) &
                                    (data[:,2]< binz[z_idx+1]+int_range_z))[0]
                else:
                    int_subdata_indices = vol_subdata_indices
                
                #loop over pairs of disctinct particles
                for k in vol_subdata_indices:
                    for l in int_subdata_indices:
                        if k!=l:
                            x_i_kl=data[k,i]-data[l,i]
                            
                            dist_kl_sq = (data[k,0]-data[l,0])**2+(data[k,1]-data[l,1])**2+(data[k,2]-data[l,2])**2
                            f_j_kl= G*(data[k,j]-data[l,j])/dist_kl_sq**(3/2)
                            
                            non_ideal_stress_tensor_array[x_idx,y_idx,z_idx]+= 1/d_volume*x_i_kl*f_j_kl
                   
    return non_ideal_stress_tensor_array

@nb.jit(parallel=True)
def non_ideal_stress_tensor_former(data, i, j, x_bins, y_bins, z_bins, m=1, x_lim=None, y_lim=None, z_lim=None, G=6.67e-11):
    
    """
    compute non ideal part of the stress tensor
    sigma_nonideal_ij = 1/2V sum x_i(l)-x_i(k) f_j(kl)
    mass is assumed to be unity
    """
    
    #set up the bins
    if x_lim == None:
        x_max=np.max(data[:,0])
        x_min=np.min(data[:,0])
    else :
        x_max=x_lim
        x_min=-x_lim
        
    binx = np.linspace(x_min, x_max, x_bins+1)
    x_step = binx[1]-binx[0]
    
    if y_lim == None:
        y_max=np.max(data[:,1])
        y_min=np.min(data[:,1])
    else:
        y_max=y_lim
        y_min=-y_lim
    
    biny = np.linspace(y_min, y_max, y_bins+1)
    y_step = biny[1]-biny[0]
    
    if z_lim == None:
        z_max=np.max(data[:,2])
        z_min=np.min(data[:,2])
    else:
        z_max=z_lim
        z_min=-z_lim
    
    binz = np.linspace(z_min, z_max, z_bins+1)
    z_step = binz[1]-binz[0]
    
    #elementary bin volume
    d_volume = x_step*y_step*z_step
    
    #compute non ideal stress tensor
    non_ideal_stress_tensor_array = np.zeros((x_bins, y_bins, z_bins))
    
    for x_idx in nb.prange(x_bins):
        for y_idx in nb.prange(y_bins):
            for z_idx in nb.prange(z_bins):
                
                #select data in the given spatial bin
                subdata = data[(data[:,0]> binx[x_idx]) &
                               (data[:,0]< binx[x_idx+1]) &
                               (data[:,1]> biny[y_idx]) &
                               (data[:,1]< biny[y_idx+1]) &
                               (data[:,2]> binz[z_idx]) &
                               (data[:,2]< binz[z_idx+1] ), :]
                
                #loop over pairs of disctinct particles
                for k in nb.prange(subdata.shape[0]):
                    for l in nb.prange(k):
                        x_i_kl=subdata[k,i]-subdata[l,i]
                        
                        dist_kl_sq = (subdata[k,0]-subdata[l,0])**2+(subdata[k,1]-subdata[l,1])**2+(subdata[k,2]-subdata[l,2])**2
                        f_j_kl= G*m*m*(subdata[k,j]-subdata[l,j])/dist_kl_sq**(3/2)
                        
                        non_ideal_stress_tensor_array[x_idx,y_idx,z_idx]+= 1/d_volume*x_i_kl*f_j_kl
                   
    return non_ideal_stress_tensor_array

@nb.jit(parallel=True)
def smoothed_non_ideal_stress_tensor(data, i, j, x_bins, y_bins, z_bins, m=1, x_lim=None, y_lim=None, z_lim=None, G=6.67e-11):
    
    """
    use neighbouring bins to compute the value of the stress tensor
    """
    
    #set up the bins
    if x_lim == None:
        x_max=np.max(data[:,0])
        x_min=np.min(data[:,0])
    else :
        x_max=x_lim
        x_min=-x_lim
        
    binx = np.linspace(x_min, x_max, x_bins+1)
    x_step = binx[1]-binx[0]
    
    if y_lim == None:
        y_max=np.max(data[:,1])
        y_min=np.min(data[:,1])
    else:
        y_max=y_lim
        y_min=-y_lim
    
    biny = np.linspace(y_min, y_max, y_bins+1)
    y_step = biny[1]-biny[0]
    
    if z_lim == None:
        z_max=np.max(data[:,2])
        z_min=np.min(data[:,2])
    else:
        z_max=z_lim
        z_min=-z_lim
    
    binz = np.linspace(z_min, z_max, z_bins+1)
    z_step = binz[1]-binz[0]
    
    #elementary bin volume
    d_volume = x_step*y_step*z_step
    
    #compute non ideal stress tensor
    non_ideal_stress_tensor_array = np.zeros((x_bins, y_bins, z_bins))
    
    for x_idx in nb.prange(x_bins):
        for y_idx in nb.prange(y_bins):
            for z_idx in nb.prange(z_bins):
                
                #select data in the given spatial bin
                subdata = data[(data[:,0]> binx[x_idx]-x_step) &
                               (data[:,0]< binx[x_idx+1]+x_step) &
                               (data[:,1]> biny[y_idx]-y_step) &
                               (data[:,1]< biny[y_idx+1]+y_step) &
                               (data[:,2]> binz[z_idx]-z_step) &
                               (data[:,2]< binz[z_idx+1]+z_step), :]
                
                #loop over pairs of disctinct particles
                for k in nb.prange(subdata.shape[0]):
                    for l in nb.prange(k):
                        x_i_kl=subdata[k,i]-subdata[l,i]
                        
                        dist_kl_sq = (subdata[k,0]-subdata[l,0])**2+(subdata[k,1]-subdata[l,1])**2+(subdata[k,2]-subdata[l,2])**2
                        f_j_kl= G*m*m*(subdata[k,j]-subdata[l,j])/dist_kl_sq**(3/2)
                        
                        non_ideal_stress_tensor_array[x_idx,y_idx,z_idx]+= 1/d_volume*x_i_kl*f_j_kl
                   
    return non_ideal_stress_tensor_array

def binner(data, x_bins, y_bins, z_bins, pmesh = True, x_lim=None,y_lim=None, z_lim=None):
    """
    returns bins for plotting, for contourf or pcolormesh
    """
   
    if x_lim == None:
        x_max=np.max(data[:,0])
        x_min=np.min(data[:,0])
    else :
        x_max=x_lim
        x_min=-x_lim
    
    if y_lim == None:
        y_max=np.max(data[:,1])
        y_min=np.min(data[:,1])
    else:
        y_max=y_lim
        y_min=-y_lim
    
    if z_lim == None:
        z_max=np.max(data[:,2])
        z_min=np.min(data[:,2])
    else:
        z_max=z_lim
        z_min=-z_lim
    
    if pmesh :
        x_bins+=1
        y_bins+=1
        z_bins+=1
    
    binx = np.linspace(x_min, x_max, x_bins)
    biny = np.linspace(y_min, y_max, y_bins) 
    binz = np.linspace(z_min, z_max, z_bins)
    
    return binx, biny, binz

def color_plot_to_radial_profile(biny, binz, data_array):

    """
    use center bins (same dimensions as data array)
    r=0 is assumed to be at the center bin ; (odd number)
    """
    
    y_grid, z_grid = np.meshgrid(biny, binz)
    radii_grid = np.sqrt(y_grid**2+z_grid**2).transpose()
    
    k_center = np.size(biny)//2
    l_center = np.size(binz)//2

   
    
    radius_bins = np.linspace(0, np.amax(radii_grid), np.size(biny)//3+1)

    radial_profile = np.zeros(np.size(biny)//3+1)
    radial_profile[0] = data_array[k_center, l_center]
    
    #print(data_array)
    
    for i in range(1, np.size(radius_bins)):
        radial_profile[i] = np.mean(data_array[(radii_grid>radius_bins[i-1])&(radii_grid<=radius_bins[i])])
    
    # print(radius_bins, radial_profile)
    return radius_bins, radial_profile
        
    
    
    

def color_plot_to_radial_profile_former(binx, biny, data, quantity, bin_number=50, plot_lim=None):
    """
    !!! previous version
    convert a 2D plot to a radially averaged profile 
    each bin has same number of particles
    (bins are off by one probably?)
    implementation can be improved
    """
  
    radius =np.sqrt(data[:,1]**2+data[:,2]**2) #r=y^2+z^2 ; projected radius

    #bin
    number_particles = data.shape[0]
    particles_per_bin = number_particles//bin_number #each bin has same number of particles
    
    #sort by radius
    sort_order =np.argsort(radius) 
    radius_sorted=radius[sort_order]    
    
    
    radius_mean = np.zeros(bin_number-1)
    
    if plot_lim is None and bin_number>=50 :
        radius_bins = np.zeros(bin_number)
        for i in range(1,bin_number): #impose zero first value at r=0
            radius_bins[i]=radius_sorted[i*particles_per_bin]
            radius_mean[i-1]=np.mean(radius_sorted[(i-1)*particles_per_bin:i*particles_per_bin])
    else:
        radius_bins = np.linspace(0, plot_lim, bin_number)
        radius_mean = np.linspace(0, plot_lim, bin_number-1)
        
    radial_profile = np.zeros(np.size(quantity)) #store quantity values in 1D array
    radial_coord = np.zeros(np.size(quantity))
    
    for i in range(np.size(binx)-1):
        for j in range(np.size(biny)-1):
            radial_coord[i*(np.size(biny)-1)+j]=np.sqrt(((binx[i]+binx[i+1])/2)**2+((biny[j]+biny[j+1])/2)**2)
            radial_profile[i*(np.size(biny)-1)+j]=quantity[i,j]
    
    radial_profile_binned=np.zeros(bin_number-1)
    
    with warnings.catch_warnings():
        warnings.filterwarnings(action='ignore', message='Mean of empty slice')
        for i in range(bin_number-1):
            radial_profile_binned[i]=np.mean(radial_profile[(radial_coord>=radius_bins[i])&(radial_coord<radius_bins[i+1])])
    
    return radius_mean, radial_profile_binned

@nb.jit(parallel=True)
def non_ideal_stress_tensor_bis(data, i, j, x_bins, y_bins, z_bins, m=1, y_lim=None, z_lim=None, G=6.67e-11):
    
    """
    compute non ideal part of the stress tensor
    sigma_nonideal_ij = 1/2V sum x_i(l)-x_i(k) f_j(kl)
    mass is assumed to be unity
    with "moving" contributions for each particle 
    """
    
    #set up the bins
    x_max=np.max(data[:,0])
    x_min=np.min(data[:,0])
    
    binx = np.linspace(x_min, x_max, x_bins+1)
    x_step = binx[1]-binx[0]
    
    if y_lim == None:
        y_max=np.max(data[:,1])
        y_min=np.min(data[:,1])
    else:
        y_max=y_lim
        y_min=-y_lim
    
    biny = np.linspace(y_min, y_max, y_bins+1)
    y_step = biny[1]-biny[0]
    
    if z_lim == None:
        z_max=np.max(data[:,2])
        z_min=np.min(data[:,2])
    else:
        z_max=z_lim
        z_min=-z_lim
    
    binz = np.linspace(z_min, z_max, z_bins+1)
    z_step = binz[1]-binz[0]
    
    #elementary bin volume
    d_volume = x_step*y_step*z_step
    
    #compute non ideal stress tensor
    int_range_x = x_step/2
    int_range_y = y_step/2
    int_range_z = z_step/2
    
    nb_particles = np.shape(data)[0]
    contribution_array = np.zeros(nb_particles)
   
    
    for k in nb.prange(nb_particles):
        subdata_indices = np.nonzero((data[k,0]-int_range_x < data[:,0]) & #get particles within interaction range
                     (data[k,0]+int_range_x > data[:,0]) &
                     (data[k,1]-int_range_y < data[:,1]) &
                     (data[k,1]+int_range_y > data[:,1]) &
                     (data[k,2]-int_range_z < data[:,2]) &
                     (data[k,2]+int_range_z > data[:,2]))
        
        #loop over pairs of disctinct particles
        for l_idx in nb.prange(np.shape(subdata_indices)[0]):
                l = subdata_indices[l_idx]
                x_i_kl=data[k,i]-data[l,i]
                dist_kl_sq = (data[k,0]-data[l,0])**2+(data[k,1]-data[l,1])**2+(data[k,2]-data[l,2])**2
                f_j_kl= G*m*m*(data[k,j]-data[l,j])/dist_kl_sq**(3/2)
                
                contribution_array[k]+= 1/d_volume*x_i_kl*f_j_kl
               
    non_ideal_stress_tensor_array = np.zeros((x_bins, y_bins, z_bins))
    for x_idx in nb.prange(x_bins):
        for y_idx in nb.prange(y_bins):
            for z_idx in nb.prange(z_bins):
                
                #select data in the given spatial bin
                subdata = contribution_array[(data[:,0]> binx[x_idx]) &
                               (data[:,0]< binx[x_idx+1]) &
                               (data[:,1]> biny[y_idx]) &
                               (data[:,1]< biny[y_idx+1]) &
                               (data[:,2]> binz[z_idx]) &
                               (data[:,2]< binz[z_idx+1] ), :]
                
                non_ideal_stress_tensor_array[x_idx,y_idx,z_idx]+= np.sum(subdata)
                   
    return non_ideal_stress_tensor_array

