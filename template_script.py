#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 16:46:07 2022

@author: pw
"""

import n_body_class
import dataVoglis

#%% computation settings

keys = ["QR1", "QR2", "QR3", "QR4"]
keys = ["QR4"]

x_bins, y_bins, z_bins = 1, 3, 3
x_lim, y_lim, z_lim = 2/25, 4, 4
neighbours = True

#write path
output_path = 'sigma_Voglis.hdf5'

#plot settings
x_slice = x_bins//2

particles = 'on'

lin_thresh = 1e-16 #17 for ni QR4 vertically avg ; 
vmax = 1e-12 #14 for ni QR4 vertically avg ; 

vmin_id_diag, vmax_id_diag = -15, -10.5
vmax_ni_diag, vmin_ni_diag = -11.5, -16

vmax_id_nondiag, lin_thresh_id = 1e-12, 1e-14
vmax_ni_nondiag, lin_thresh_ni = 1e-13, 1e-17

close_plot, save, save_path = False, True, 'figures'
#%%

#import output data from simulation
datadict = dataVoglis.datadict
mass_one_particle = dataVoglis.mass_one_particle
velocity_factor_si_units = dataVoglis.velocity_factor_si_units
Rh = dataVoglis.RhVoglis

sigmalist = []
for key in keys:
    #calculation
    sigma = n_body_class.n_body_data(key, datadict[key])
    sigma.compute_sigma(x_bins, y_bins, z_bins, x_lim, y_lim, z_lim, neighbours = neighbours)
    sigma.convert_to_si_units(mass_one_particle, velocity_factor_si_units, Rh)
    
 
    
    #write
    sigmalist.append(sigma)
    sigma.write_sigma(output_path)

    #plot
    sigma.plot_suite(vmin_id_diag, vmax_id_diag, 
                        vmax_id_nondiag, lin_thresh_id, 
                        vmax_ni_diag, vmin_ni_diag,
                        vmax_ni_nondiag, lin_thresh_ni, 
                        particles = particles,
                        x_slice = x_slice,
                        close_plot = close_plot, save = save, save_path = save_path)
    
    sigma.comparison_plot()